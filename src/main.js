import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHashHistory } from 'vue-router'
import HomePage from "./components/pages/HomePage.vue";
import ProjectsPage from "./components/pages/ProjectsPage.vue";
import ResumePage from "./components/pages/ResumePage.vue";
import ContactPage from "./components/pages/ContactPage.vue";

const routes  = [
    { path: '/', component: HomePage},
    { path: '/project', component: ProjectsPage},
    { path: '/resume', component: ResumePage},
    { path: '/contact', component: ContactPage},
];

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes,
});

createApp(App)
    .use(router)
    .mount('#app')
